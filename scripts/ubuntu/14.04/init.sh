#!/bin/bash

user=$(whoami)
if [[ "$user" != "root" ]]; then
    echo "please run the script as root"
    exit
fi

usage()
{
cat << EOF
usage: $0 options

This script sets up a server machine.

OPTIONS:
   -h      Show this message
   -t      Update aptitude
   -d	   Upgrade all packages
   -n	   Hostname of Server
   -e      Setup mail from server
   -a      Install Apache
   -p      Install PHP
   -g      Install Git
   -m      Install Mysql and Specify Mysql Password
   -f      Install Fail2Ban and configure
   -j      Install Java 8
   -i      Install Imagemagick

Example - $0 -t y -d y -n "www.example.com" -e y -a y -p y -g y -m mysqlpassword -f y -j n -i y

EOF
}


UPDATE=		#	-t
UPGRADE=  	#	-d
HOST=		#	-n
MAIL=		#	-e
APACHE=		#	-a
PHP=		#	-p
GIT=		#	-g
MYSQLPASS=	# 	-m
FAIL2BAN=	# 	-f
JAVA=		# 	-j
IMAGEMAGICK=	#	-i

while getopts "ht:d:n:e:a:p:g:m:f:j:i:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         t)
             UPDATE=$OPTARG
             ;;
         d)
             UPGRADE=$OPTARG
             ;;
         n)
             HOST=$OPTARG
             ;;
         e)
             MAIL=$OPTARG
             ;;
	 a) 
 	     APACHE=$OPTARG
	     ;;
	 p)
	     PHP=$OPTARG
             ;;
	 g)
	     GIT=$OPTARG
	     ;;
	 m)
	     MYSQLPASS=$OPTARG
	     ;;
	 f)
	     FAIL2BAN=$OPTARG
	     ;;
	 j)
	     JAVA=$OPTARG
	     ;;
	 c)
	     DEPLOYMENT=$OPTARG
	     ;;
	 i) 
             IMAGEMAGICK=$OPTARG
	     ;;
	 ?)
             usage
             exit
             ;;
     esac
done


if [[ -z $UPDATE ]]
then
	read -p "Do you want to update the system(y/n) " UPDATE
fi

if [[ -z $UPGRADE ]]
then
	read -p "Do you want to upgrade the system(y/n) " UPGRADE
fi

if [[ -z $HOST ]]
then
	read -p "Enter a new hostname-- \n" HOST
fi

if [[ -z $MAIL ]]
then
	read -p "Do you want to configure the system for mails(y/n) " MAIL
fi

if [[ -z $APACHE ]]
then
	read -p "Do you want to install apache2(y/n) " APACHE
fi

if [[ -z $PHP ]]
then
	read -p "Do you want to install php5(y/n) " PHP
fi

if [[ -z $GIT ]]
then
	read -p "Do you want to install git(y/n) " GIT
fi

if [[ -z $MYSQLPASS ]]
then
	read -p "Do you want to install mysql-client(y/n) " MYSQLPASS
		if [[ "$MYSQLPASS" = "y" ]]; then
			read -p "Please enter password\n" MYSQLPASS
		fi
fi

if [[ -z $FAIL2BAN ]]
then
	read -p "Do you want to install fail2ban(y/n) " FAIL2BAN
fi

if [[ -z $JAVA ]]
then
	read -p "Do you want to install java 8(y/n) " JAVA
fi

if [[ -z $IMAGEMAGICK ]]
then
        read -p "Do you want to Instal Imagemagick(y/n) " IMAGEMAGICK
fi


sudo apt-get install -y aptitude
sudo aptitude install -y vim
sudo aptitude install -y dpkg-dev


if [[ "$UPDATE" = "y" ]]; then
	sudo aptitude update
fi

if [[ "$UPGRADE" = "y" ]]; then
	sudo DEBIAN_FRONTEND=noninteractive aptitude -y dist-upgrade
fi

if [[ ! -z "$HOST" ]]; then
	echo "$HOST" > /etc/hostname
	echo "127.0.0.1 $HOST" >> /etc/hosts
fi

if [[ "$MAIL" = "y" ]]; then
	sudo DEBIAN_FRONTEND=noninteractive aptitude install -y -q mailutils
fi

if [[ "$APACHE" = "y" ]]; then
	sudo aptitude install -y apache2
fi

if [[ "$PHP" = "y" ]]; then
	sudo aptitude install -y php5
	sudo aptitude install -y curl
	sudo aptitude install -y libcurl3
	sudo aptitude install -y libcurl3-dev
	sudo aptitude install -y php5-curl
	sudo aptitude install -y php5-mysql
	sudo aptitude install -y libapache2-mod-php5
        sudo aptitude install -y php5-gd
	sudo aptitude install -y php5-dev
	sudo aptitude install -y php-apc
	sudo aptitude install -y php5-cli 
	sudo aptitude install -y php5-json
	sudo a2dissite 000-default
	sudo service apache2 restart
fi

if [[ "$GIT" = "y" ]]; then
	sudo aptitude install -y git
fi

if [[ ! -z "$MYSQLPASS"  ]]; then
	sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQLPASS"
	sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQLPASS"
	sudo aptitude -y install mysql-server
fi	
	
if [[ "$FAIL2BAN" = "y" ]]; then
	sudo aptitude install -y fail2ban
	cp $HOME/deployment/scripts/jail.local /etc/fail2ban/jail.local
fi

if [[ "$JAVA" = "y" ]]; then
	sudo add-apt-repository ppa:webupd8team/java
	sudo aptitude update
	echo debconf shared/accepted-oracle-license-v1-1 select true | \sudo debconf-set-selections
	echo debconf shared/accepted-oracle-license-v1-1 seen true | \sudo debconf-set-selections
	sudo aptitude install -y oracle-java8-installer 
	sudo aptitude install -y oracle-java8-set-default
fi

if [[ "$IMAGEMAGICK" = "y" ]]; then
	sudo aptitude install -y imagemagick	
fi
