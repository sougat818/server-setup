#!/bin/bash

source $(dirname $0)/utils.sh

HOST="facebook.com"
USER="phabricator"

while getopts "h:u:" OPTION
do
	case $OPTION in
		h)
			HOST=$OPTARG
             		;;
         
		u)
             		USER=$OPTARG
             		;;
	esac
done


URL=$USER.$HOST
user=$(whoami)
if [[ "$user" != "root" ]]; then

	if [[ "$user" == "$USER" ]]; then
		cd $HOME
		echo "Installing in `pwd`"

		if [ ! -e libphutil ]
		then
		  	git clone https://github.com/phacility/libphutil.git
		else
  			(cd libphutil && git pull --rebase)
		fi

		if [ ! -e arcanist ]
		then
  			git clone https://github.com/phacility/arcanist.git
		else
			(cd arcanist && git pull --rebase)
		fi

		if [ ! -e phabricator ]
		then
 			git clone https://github.com/phacility/phabricator.git
		else
  			(cd phabricator && git pull --rebase)
		fi	
		exit 0 ;
	else
		echo "Invalid User. Either run as Root or $USER."
		exit;
	fi

fi

usage()
{
cat << EOF
usage: $0 options

EOF
}

mysqlrootpassword=$(genpass);
mysqluserpassword=$(genpass);
userpass=$(genpass);
./init.sh -t y -d y -n "$URL" -e y -a y -p y -g y -m $mysqlrootpassword -f n -j n -i n

sudo a2enmod rewrite
sudo adduser --disabled-password --gecos "" $USER 
usermod -p `openssl passwd -1 userpass` $USER

sudo su $USER -c "$0 -h $HOST -u $USER"

cat <<EOT >> /etc/apache2/sites-available/$URL.conf
<VirtualHost *>
  ServerName ben10.ailien.com
  DocumentRoot /home/ben10/phabricator/webroot
  RewriteEngine on
  RewriteRule ^/rsrc/(.*)     -                       [L,QSA]
  RewriteRule ^/favicon.ico   -                       [L,QSA]
  RewriteRule ^(.*)\$          /index.php?__path__=\$1  [B,L,QSA]
        <Directory "/home/ben10/phabricator/webroot">
        	Require all granted
        </Directory>
ErrorLog \${APACHE_LOG_DIR}/error.log
LogLevel debug
CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOT

sudo service apache2 stop

mysql -uroot -p$mysqlrootpassword -e "CREATE USER '$USER'@'localhost' IDENTIFIED BY '$mysqluserpassword'"
mysql -uroot -p$mysqlrootpassword -e "GRANT ALL PRIVILEGES ON *.* TO '$USER'@'localhost'"

/home/$USER/phabricator/bin/config set mysql.user $USER
/home/$USER/phabricator/bin/config set mysql.pass $mysqluserpassword
/home/$USER/phabricator/bin/storage --force  upgrade

sudo a2ensite $URL
sudo service apache2 restart

echo "Mysql Root password - $mysqlrootpassword"
echo "$USER password = $userpass"
echo "Mysql $USER password = $mysqluserpassword"
